export class CountryTransportLayer {
    getCountries = async (): Promise < any > => {
        let countries: any = [];
        await fetch("https://restcountries.eu/rest/v2/all")
            .then(response => response.json())
            .then((result) => {
                    countries = result;
                },
                (error) => {
                    // ? handle http error;
                })
        return countries;
    }
}